﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ScientificCalculator
{

    public static class Logic
    {
        public static string inputBufferPattern = @"^[0-9\-(] $";
        public static string inputPattern = @"^[0-9\-(] $";
        public static Stack<char> operatorStack = new Stack<char>();
        public static Queue<string> postfixExpressionQueue = new Queue<string>();
        public static double Result { get; set; }
        public static double Memory { get; set; }
        public static bool IsMemoryEngaged { get; set; }

        public static bool ValidateInputBuffer(string inputBuffer)
        {           
            return true;
        }
        public static bool ValidateInput(string input)
        {
            return true;
        }
        public static void ConvertToPostFix(string infixExpression)
        {
            string operand = string.Empty;
            operatorStack.Clear();
            postfixExpressionQueue.Clear();
            if (!ValidateInput(infixExpression))
            {
                throw new Exception("Invalid input.");
            }
            foreach (char character in infixExpression)
            {
                //Flush the previous operand to the postfix expression stack.
                if (char.IsLetter(character) | character.Equals(')')) 
                {
                    if (!string.IsNullOrWhiteSpace(operand))
                    {
                        postfixExpressionQueue.Enqueue(operand);
                        operand = string.Empty;
                    }
                }
                switch (character)
                {
                    case '(':
                        {
                            operatorStack.Push(character);
                            break;
                        }
                    case ')':
                        {
                            while (operatorStack.Count > 0)
                            {
                                char tempChar = operatorStack.Pop();
                                if (tempChar != '(')
                                {
                                    postfixExpressionQueue.Enqueue(tempChar.ToString());
                                }
                                else
                                {
                                    break;
                                }
                            }
                            break;
                        }

                    case 'A':   //Addition		
                    case 'B':   //Subtraction			
                    case 'C':   //Multiplication		
                    case 'D':   //Division		
                    case 'E':   //Sine			
                    case 'F':   //arcSine			
                    case 'G':   //Cosine				
                    case 'H':   //arcCosine		
                    case 'I':   //Tangent			
                    case 'J':   //arcTangent			
                    case 'K':   //Negation			
                    case 'L':   //Root				
                    case 'M':   //Square			
                    case 'N':   //Power			
                    case 'O':   //nth root			
                    case 'P':   //Natural log			
                    case 'Q':   //Natural exp		
                    case 'R':   //Base 10 log			
                    case 'S':   //Exponent			
                    case 'T':   //Inverse			
                    case 'U':   //Factorial		
                        {
                            if (operatorStack.Count == 0)
                            {
                                operatorStack.Push(character);
                            }
                            else
                            {
                                while (operatorStack.Count > 0)
                                {
                                    char tempChar = operatorStack.Pop();
                                    if (tempChar == '(')
                                    {
                                        operatorStack.Push(tempChar);
                                    }
                                    else
                                    {
                                        if (tempChar < character) //this uses ASCII code values to  measure operator precedence
                                        {
                                            operatorStack.Push(tempChar);
                                        }
                                        else
                                        {
                                            postfixExpressionQueue.Enqueue(tempChar.ToString());
                                        }
                                    }
                                    if((tempChar == '(')|(tempChar < character))
                                    {
                                        break;
                                    }
                                }
                                operatorStack.Push(character);
                            }
                            break;
                        }
                    default:
                        {
                            //Collect characters (digits and/or comma) making up the current operand.
                            operand += character;
                            break;
                        }                       
                }
            }
            //Flush the last operand to the postfix expression stack.
            if (!string.IsNullOrWhiteSpace(operand))
            {
                postfixExpressionQueue.Enqueue(operand);
            }
            while (operatorStack.Count > 0)
            {
                postfixExpressionQueue.Enqueue(operatorStack.Pop().ToString());
            }
        } 
        public static double EvaluateExpression(string infixExpression)
        {
            Stack<double> operandStack = new Stack<double>();
            double firstOperand;
            double secondOperand = default(double);

            try
            {
                ConvertToPostFix(infixExpression);
            }
            catch (Exception)
            {
                throw;
            }
            while(postfixExpressionQueue.Count > 0)
            {
                string currentItem = postfixExpressionQueue.Dequeue();
                char currentOpCode;
                if(char.TryParse(currentItem, out currentOpCode) & (char.IsLetter(currentOpCode)))
                {
                    CustomOperatorType currentOperator;
                    try
                    {
                        currentOperator = CustomOperators.GetCustomOperatorType(currentOpCode);
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                    try
                    {
                        firstOperand = operandStack.Pop();
                    }
                    catch (Exception)
                    {
                        throw new Exception("Operand not provided.");
                    }
                    if (currentOperator.IsBinaryOperator) //Check if binary operator 
                    {

                        try
                        {
                            secondOperand = operandStack.Pop();
                        }
                        catch (Exception)
                        {
                            throw new Exception("Second operand not provided.");
                        }
                    }
                    double interimResult = currentOperator.OperatorFunction(new[] { firstOperand, secondOperand });
                    operandStack.Push(interimResult);
                }
                else
                {
                    operandStack.Push(Convert.ToDouble(currentItem));
                }
            }

            try
            {
                Result = operandStack.Pop();
                return Result;
            }
            catch (Exception)
            {
                throw new Exception("Insufficient operands provided.");
            }
        }
    }
}
