﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScientificCalculator
{
    public class ShiftButton : DefaultStyleButton
    {
        public bool IsSet { get; set; }

        public event EventHandler ShiftToggled;

        public ShiftButton() : base() { }

        public void OnShiftToggled()
        {
            ShiftToggled?.Invoke(this, new EventArgs());
        }
    }
}
