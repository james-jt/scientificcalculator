﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScientificCalculator
{
    public class OperatorButton : DefaultStyleButton
    {
        public CustomOperatorType Operator { get; set; }
        public CustomOperatorType NormalOperator { get; private set; }
        public CustomOperatorType ShiftOperator { get; private set; }

        public OperatorButton(CustomOperatorType _normalOperator, CustomOperatorType _shiftOperator) : base()
        {
            NormalOperator = _normalOperator;
            ShiftOperator = _shiftOperator;
            Operator = NormalOperator;
        }
    }
}
