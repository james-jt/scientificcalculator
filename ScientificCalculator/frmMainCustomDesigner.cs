﻿
namespace ScientificCalculator
{
    partial class frmMain
    {
        private void InitializeCustomComponent()
        {
            this.btnOpenBracket = new OperatorButton(customOperators.OpenBracketOperator, customOperators.OpenBracketOperator);
            this.btnCloseBracket = new OperatorButton(customOperators.CloseBracketOperator, customOperators.CloseBracketOperator);
            this.btnDivision = new OperatorButton(customOperators.DivisionOperator, customOperators.DivisionOperator);
            this.btnMultiplication = new OperatorButton(customOperators.MultiplicationOperator, customOperators.MultiplicationOperator);
            this.btnAddition = new OperatorButton(customOperators.AdditionOperator, customOperators.AdditionOperator);
            this.btnSubtraction = new OperatorButton(customOperators.SubtractionOperator, customOperators.SubtractionOperator);
            this.btnNegation = new OperatorButton(customOperators.NegationOperator, customOperators.PercentageOperator);
            this.btnCosine = new OperatorButton(customOperators.CosineOperator, customOperators.arcCosineOperator);
            this.btnTangent = new OperatorButton(customOperators.TangentOperator, customOperators.arcTangentOperator);
            this.btnBase10log = new OperatorButton(customOperators.Base10LogOperator, customOperators.ExponentOperator);
            this.btnSine = new OperatorButton(customOperators.SineOperator, customOperators.arcSineOperator);
            this.btnRoot = new OperatorButton(customOperators.SRootOperator, customOperators.SRootOperator);
            this.btnNthRoot = new OperatorButton(customOperators.NRootOperator, customOperators.NRootOperator);
            this.btnSquare = new OperatorButton(customOperators.SquareOperator, customOperators.SquareOperator);
            this.btnInverse = new OperatorButton(customOperators.InverseOperator, customOperators.InverseOperator);
            this.btnCube = new OperatorButton(customOperators.CubeOperator, customOperators.CubeOperator);
            this.btnPowerOfN = new OperatorButton(customOperators.PowerOperator, customOperators.PowerOperator);
            this.btnNaturalLog = new OperatorButton(customOperators.NaturalLogOperator, customOperators.NaturalExpOperator);
            this.btnFactorial = new OperatorButton(customOperators.FactorialOperator, customOperators.FactorialOperator);
            this.btnCubeRoot = new OperatorButton(customOperators.CubeRootOperator, customOperators.CubeRootOperator);
            this.btnMemoryPlus = new OperatorButton(customOperators.AdditionOperator, customOperators.AdditionOperator);
            this.btnShift = new ShiftButton();
            this.btnPi = new ConstantButton(System.Math.PI);

            // 
            // btnOpenBracket
            // 
            this.btnOpenBracket.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenBracket.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnOpenBracket.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnOpenBracket.Location = new System.Drawing.Point(145, 6);
            this.btnOpenBracket.Name = "btnOpenBracket";
            this.btnOpenBracket.Size = new System.Drawing.Size(41, 30);
            this.btnOpenBracket.TabIndex = 15;
            this.btnOpenBracket.Text = this.btnOpenBracket.Operator.OperatorCaption;
            this.btnOpenBracket.UseVisualStyleBackColor = false;
            this.btnOpenBracket.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnOpenBracket);

            // 
            // btnCloseBracket
            // 
            this.btnCloseBracket.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseBracket.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnCloseBracket.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCloseBracket.Location = new System.Drawing.Point(192, 6);
            this.btnCloseBracket.Name = "btnCloseBracket";
            this.btnCloseBracket.Size = new System.Drawing.Size(41, 30);
            this.btnCloseBracket.TabIndex = 18;
            this.btnCloseBracket.Text = this.btnCloseBracket.Operator.OperatorCaption;
            this.btnCloseBracket.UseVisualStyleBackColor = false;
            this.btnCloseBracket.Click += btnCloseBracket_Click;
            this.OperatorButtons.Add(this.btnCloseBracket);

            //
            // btnCosine
            //
            this.btnCosine.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCosine.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnCosine.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCosine.Location = new System.Drawing.Point(51, 129);
            this.btnCosine.Name = "btnCosine";
            this.btnCosine.Size = new System.Drawing.Size(42, 23);
            this.btnCosine.TabIndex = 46;
            this.btnCosine.Text = this.btnCosine.Operator.OperatorCaption;
            this.btnCosine.UseVisualStyleBackColor = false;
            this.btnCosine.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnCosine);

            // 
            // btnTanget
            // 
            this.btnTangent.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTangent.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnTangent.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnTangent.Location = new System.Drawing.Point(98, 129);
            this.btnTangent.Name = "btnTanget";
            this.btnTangent.Size = new System.Drawing.Size(41, 23);
            this.btnTangent.TabIndex = 45;
            this.btnTangent.Text = this.btnTangent.Operator.OperatorCaption; 
            this.btnTangent.UseVisualStyleBackColor = false;
            this.btnTangent.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnTangent);

            // 
            // btnBase10Log
            // 
            this.btnBase10log.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBase10log.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnBase10log.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnBase10log.Location = new System.Drawing.Point(145, 129);
            this.btnBase10log.Name = "btnBase10Log";
            this.btnBase10log.Size = new System.Drawing.Size(41, 23);
            this.btnBase10log.TabIndex = 44;
            this.btnBase10log.Text = this.btnBase10log.Operator.OperatorCaption;
            this.btnBase10log.UseVisualStyleBackColor = false;
            this.btnBase10log.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnBase10log);

            // 
            // btnSine
            // 
            this.btnSine.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSine.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnSine.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSine.Location = new System.Drawing.Point(4, 129);
            this.btnSine.Name = "btnSine";
            this.btnSine.Size = new System.Drawing.Size(41, 23);
            this.btnSine.TabIndex = 43;
            this.btnSine.Text = this.btnSine.Operator.OperatorCaption;
            this.btnSine.UseVisualStyleBackColor = false;
            this.btnSine.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnSine);

            // 
            // btnSquareRoot
            // 
            this.btnRoot.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoot.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnRoot.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnRoot.Location = new System.Drawing.Point(4, 71);
            this.btnRoot.Name = "btnSquareRoot";
            this.btnRoot.Size = new System.Drawing.Size(41, 23);
            this.btnRoot.TabIndex = 42;
            this.btnRoot.Text = this.btnRoot.Operator.OperatorCaption;
            this.btnRoot.UseVisualStyleBackColor = false;
            this.btnRoot.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnRoot);

            // 
            // btnNthRoot
            // 
            this.btnNthRoot.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNthRoot.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnNthRoot.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnNthRoot.Location = new System.Drawing.Point(4, 42);
            this.btnNthRoot.Name = "btnNthRoot";
            this.btnNthRoot.Size = new System.Drawing.Size(41, 23);
            this.btnNthRoot.TabIndex = 41;
            this.btnNthRoot.Text = this.btnNthRoot.Operator.OperatorCaption;
            this.btnNthRoot.UseVisualStyleBackColor = false;
            this.btnNthRoot.Enabled = true;
            this.btnNthRoot.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnNthRoot);


            // 
            // btnSquare
            // 
            this.btnSquare.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSquare.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnSquare.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSquare.Location = new System.Drawing.Point(51, 71);
            this.btnSquare.Name = "btnSquare";
            this.btnSquare.Size = new System.Drawing.Size(41, 23);
            this.btnSquare.TabIndex = 40;
            this.btnSquare.Text = this.btnSquare.Operator.OperatorCaption; 
            this.btnSquare.UseVisualStyleBackColor = false;
            this.btnSquare.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnSquare);

            // 
            // btnInverse
            // 
            this.btnInverse.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInverse.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnInverse.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnInverse.Location = new System.Drawing.Point(145, 71);
            this.btnInverse.Name = "btnInverse";
            this.btnInverse.Size = new System.Drawing.Size(41, 23);
            this.btnInverse.TabIndex = 39;
            this.btnInverse.Text = this.btnInverse.Operator.OperatorCaption;
            this.btnInverse.UseVisualStyleBackColor = false;
            this.btnInverse.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnInverse);

            // 
            // btnCube
            // 
            this.btnCube.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCube.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnCube.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCube.Location = new System.Drawing.Point(51, 100);
            this.btnCube.Name = "btnCube";
            this.btnCube.Size = new System.Drawing.Size(41, 23);
            this.btnCube.TabIndex = 38;
            this.btnCube.Text = this.btnCube.Operator.OperatorCaption;
            this.btnCube.UseVisualStyleBackColor = false;
            this.btnCube.Enabled = true;
            this.btnCube.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnCube);

            // 
            // btnPower
            // 
            this.btnPowerOfN.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPowerOfN.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnPowerOfN.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnPowerOfN.Location = new System.Drawing.Point(98, 100);
            this.btnPowerOfN.Name = "btnPower";
            this.btnPowerOfN.Size = new System.Drawing.Size(41, 23);
            this.btnPowerOfN.TabIndex = 37;
            this.btnPowerOfN.Text = this.btnPowerOfN.Operator.OperatorCaption;
            this.btnPowerOfN.UseVisualStyleBackColor = false;
            this.btnPowerOfN.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnPowerOfN);

            // 
            // btnNaturalLog
            // 
            this.btnNaturalLog.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNaturalLog.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnNaturalLog.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnNaturalLog.Location = new System.Drawing.Point(145, 100);
            this.btnNaturalLog.Name = "btnNaturalLog";
            this.btnNaturalLog.Size = new System.Drawing.Size(41, 23);
            this.btnNaturalLog.TabIndex = 36;
            this.btnNaturalLog.Text = this.btnNaturalLog.Operator.OperatorCaption;
            this.btnNaturalLog.UseVisualStyleBackColor = false;
            this.btnNaturalLog.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnNaturalLog);

            // 
            // btnFactorial
            // 
            this.btnFactorial.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFactorial.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnFactorial.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnFactorial.Location = new System.Drawing.Point(192, 100);
            this.btnFactorial.Name = "btnFactorial";
            this.btnFactorial.Size = new System.Drawing.Size(41, 23);
            this.btnFactorial.TabIndex = 35;
            this.btnFactorial.Text = this.btnFactorial.Operator.OperatorCaption;
            this.btnFactorial.UseVisualStyleBackColor = false;
            this.btnFactorial.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnFactorial);

            // 
            // btnCubeRoot
            // 
            this.btnCubeRoot.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCubeRoot.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnCubeRoot.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCubeRoot.Location = new System.Drawing.Point(4, 100);
            this.btnCubeRoot.Name = "btnCubeRoot";
            this.btnCubeRoot.Size = new System.Drawing.Size(41, 23);
            this.btnCubeRoot.TabIndex = 34;
            this.btnCubeRoot.Text = this.btnCubeRoot.Operator.OperatorCaption;
            this.btnCubeRoot.UseVisualStyleBackColor = false;
            this.btnCubeRoot.Enabled = true;
            this.btnCubeRoot.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnCubeRoot);


            // 
            // btnNegation
            // 
            this.btnNegation.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNegation.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnNegation.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnNegation.Location = new System.Drawing.Point(98, 114);
            this.btnNegation.Name = "btnNegation";
            this.btnNegation.Size = new System.Drawing.Size(41, 30);
            this.btnNegation.TabIndex = 23;
            this.btnNegation.Text = this.btnNegation.Operator.OperatorCaption;
            this.btnNegation.UseVisualStyleBackColor = false;
            this.btnNegation.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnNegation);

            // 
            // btnSubstraction
            // 
            this.btnSubtraction.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubtraction.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnSubtraction.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSubtraction.Location = new System.Drawing.Point(192, 78);
            this.btnSubtraction.Name = "btnSubstraction";
            this.btnSubtraction.Size = new System.Drawing.Size(41, 30);
            this.btnSubtraction.TabIndex = 22;
            this.btnSubtraction.Text = this.btnSubtraction.Operator.OperatorCaption;
            this.btnSubtraction.UseVisualStyleBackColor = false;
            this.btnSubtraction.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnSubtraction);

            // 
            // btnAddition
            // 
            this.btnAddition.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddition.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnAddition.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnAddition.Location = new System.Drawing.Point(145, 78);
            this.btnAddition.Name = "btnAddition";
            this.btnAddition.Size = new System.Drawing.Size(41, 30);
            this.btnAddition.TabIndex = 21;
            this.btnAddition.Text = this.btnAddition.Operator.OperatorCaption;
            this.btnAddition.UseVisualStyleBackColor = false;
            this.btnAddition.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnAddition);

            // 
            // btnDivision
            // 
            this.btnDivision.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDivision.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnDivision.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnDivision.Location = new System.Drawing.Point(192, 42);
            this.btnDivision.Name = "btnDivision";
            this.btnDivision.Size = new System.Drawing.Size(41, 30);
            this.btnDivision.TabIndex = 20;
            this.btnDivision.Text = this.btnDivision.Operator.OperatorCaption;
            this.btnDivision.UseVisualStyleBackColor = false;
            this.btnDivision.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnDivision);

            // 
            // btnMultiplication
            // 
            this.btnMultiplication.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMultiplication.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnMultiplication.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnMultiplication.Location = new System.Drawing.Point(145, 42);
            this.btnMultiplication.Name = "btnMultiplication";
            this.btnMultiplication.Size = new System.Drawing.Size(41, 30);
            this.btnMultiplication.TabIndex = 19;
            this.btnMultiplication.Text = this.btnMultiplication.Operator.OperatorCaption;
            this.btnMultiplication.UseVisualStyleBackColor = false;
            this.btnMultiplication.Click += btnSimpleAppend_Click;
            this.OperatorButtons.Add(this.btnMultiplication);

            // 
            // btnMemoryPlus
            // 
            this.btnMemoryPlus.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnMemoryPlus.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnMemoryPlus.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMemoryPlus.Location = new System.Drawing.Point(192, 129);
            this.btnMemoryPlus.Name = "btnMemoryPlus";
            this.btnMemoryPlus.Size = new System.Drawing.Size(41, 23);
            this.btnMemoryPlus.TabIndex = 33;
            this.btnMemoryPlus.Text = "M+";
            this.btnMemoryPlus.UseVisualStyleBackColor = false;
            this.btnMemoryPlus.Enabled = true;
            this.btnMemoryPlus.Click += btnMemoryPlus_Click;

            // 
            // btnPi
            // 
            this.btnPi.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnPi.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPi.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnPi.Location = new System.Drawing.Point(98, 42);
            this.btnPi.Name = "btnPi";
            this.btnPi.Size = new System.Drawing.Size(41, 23);
            this.btnPi.TabIndex = 25;
            this.btnPi.Text = "π";
            this.btnPi.UseVisualStyleBackColor = false;
            this.btnPi.Click += btnSimpleAppend_Click;

            // 
            // btnShift
            // 
            this.btnShift.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShift.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnShift.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnShift.Location = new System.Drawing.Point(4, 13);
            this.btnShift.Name = "btnShift";
            this.btnShift.Size = new System.Drawing.Size(41, 23);
            this.btnShift.TabIndex = 32;
            this.btnShift.Text = "Shift";
            this.btnShift.UseVisualStyleBackColor = false;
            this.btnShift.Click += btnShift_Click;
            this.btnShift.ShiftToggled += AllButtons_ShiftToggled;

            // 
            // pnlBottomKeys
            // 
            this.pnlBottmKeys.Controls.Add(this.btnCloseBracket);
            this.pnlBottmKeys.Controls.Add(this.btnOpenBracket);
            this.pnlBottmKeys.Controls.Add(this.btnMultiplication);
            this.pnlBottmKeys.Controls.Add(this.btnDivision);
            this.pnlBottmKeys.Controls.Add(this.btnAddition);
            this.pnlBottmKeys.Controls.Add(this.btnSubtraction);
            this.pnlBottmKeys.Controls.Add(this.btnNegation);

            // 
            // pnlTopKeys
            // 
            this.pnlTopKeys.Controls.Add(this.btnCosine);
            this.pnlTopKeys.Controls.Add(this.btnTangent);
            this.pnlTopKeys.Controls.Add(this.btnBase10log);
            this.pnlTopKeys.Controls.Add(this.btnSine);
            this.pnlTopKeys.Controls.Add(this.btnRoot);
            this.pnlTopKeys.Controls.Add(this.btnNthRoot);
            this.pnlTopKeys.Controls.Add(this.btnSquare);
            this.pnlTopKeys.Controls.Add(this.btnInverse);
            this.pnlTopKeys.Controls.Add(this.btnCube);
            this.pnlTopKeys.Controls.Add(this.btnPowerOfN);
            this.pnlTopKeys.Controls.Add(this.btnNaturalLog);
            this.pnlTopKeys.Controls.Add(this.btnFactorial);
            this.pnlTopKeys.Controls.Add(this.btnCubeRoot);
            this.pnlTopKeys.Controls.Add(this.btnMemoryPlus);
            this.pnlTopKeys.Controls.Add(this.btnShift);
            this.pnlTopKeys.Controls.Add(this.btnPi);
        }

        private OperatorButton btnOpenBracket;
        private OperatorButton btnCloseBracket;
        private OperatorButton btnNegation;
        private OperatorButton btnSubtraction;
        private OperatorButton btnAddition;
        private OperatorButton btnDivision;
        private OperatorButton btnMultiplication;
        private OperatorButton btnCosine;
        private OperatorButton btnTangent;
        private OperatorButton btnBase10log;
        private OperatorButton btnRoot;
        private OperatorButton btnNthRoot;
        private OperatorButton btnSquare;
        private OperatorButton btnInverse;
        private OperatorButton btnCube;
        private OperatorButton btnPowerOfN;
        private OperatorButton btnNaturalLog;
        private OperatorButton btnFactorial;
        private OperatorButton btnSine;
        private OperatorButton btnCubeRoot;
        private ShiftButton btnShift;
        private ConstantButton btnPi;
    }
}