﻿namespace ScientificCalculator
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pnlKeys = new System.Windows.Forms.Panel();
            this.pnlBottmKeys = new System.Windows.Forms.Panel();
            this.btn7 = new ScientificCalculator.DefaultStyleButton();
            this.btn6 = new ScientificCalculator.DefaultStyleButton();
            this.btn5 = new ScientificCalculator.DefaultStyleButton();
            this.btn4 = new ScientificCalculator.DefaultStyleButton();
            this.btn3 = new ScientificCalculator.DefaultStyleButton();
            this.btn2 = new ScientificCalculator.DefaultStyleButton();
            this.btn1 = new ScientificCalculator.DefaultStyleButton();
            this.btnEquals = new ScientificCalculator.DefaultStyleButton();
            this.btnDecimal = new ScientificCalculator.DefaultStyleButton();
            this.btn0 = new ScientificCalculator.DefaultStyleButton();
            this.btn8 = new ScientificCalculator.DefaultStyleButton();
            this.btn9 = new ScientificCalculator.DefaultStyleButton();
            this.pnlTopKeys = new System.Windows.Forms.Panel();
            this.btnStore = new ScientificCalculator.DefaultStyleButton();
            this.btnUp = new ScientificCalculator.DefaultStyleButton();
            this.btnRecall = new ScientificCalculator.DefaultStyleButton();
            this.btnClear = new ScientificCalculator.DefaultStyleButton();
            this.btnLeft = new ScientificCalculator.DefaultStyleButton();
            this.btnRight = new ScientificCalculator.DefaultStyleButton();
            this.btnDelete = new ScientificCalculator.DefaultStyleButton();
            this.btnDown = new ScientificCalculator.DefaultStyleButton();
            this.btnDRG = new ScientificCalculator.DefaultStyleButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tspToggleDisplay = new System.Windows.Forms.ToolStripMenuItem();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.txtInputBuffer = new System.Windows.Forms.TextBox();
            this.pnlFrontPanel = new System.Windows.Forms.Panel();
            this.pnlDisplay = new System.Windows.Forms.Panel();
            this.expressionHistoryLabel5 = new ScientificCalculator.ExpressionHistoryLabel();
            this.expressionHistoryLabel4 = new ScientificCalculator.ExpressionHistoryLabel();
            this.expressionHistoryLabel3 = new ScientificCalculator.ExpressionHistoryLabel();
            this.expressionHistoryLabel2 = new ScientificCalculator.ExpressionHistoryLabel();
            this.expressionHistoryLabel1 = new ScientificCalculator.ExpressionHistoryLabel();
            this.lblMemory = new System.Windows.Forms.Label();
            this.lblShift = new System.Windows.Forms.Label();
            this.lblDRG = new System.Windows.Forms.Label();
            this.pnlKeys.SuspendLayout();
            this.pnlBottmKeys.SuspendLayout();
            this.pnlTopKeys.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.pnlFrontPanel.SuspendLayout();
            this.pnlDisplay.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlKeys
            // 
            this.pnlKeys.Controls.Add(this.pnlBottmKeys);
            this.pnlKeys.Controls.Add(this.pnlTopKeys);
            this.pnlKeys.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlKeys.Location = new System.Drawing.Point(0, 188);
            this.pnlKeys.Name = "pnlKeys";
            this.pnlKeys.Size = new System.Drawing.Size(238, 305);
            this.pnlKeys.TabIndex = 4;
            // 
            // pnlBottmKeys
            // 
            this.pnlBottmKeys.BackColor = System.Drawing.Color.Transparent;
            this.pnlBottmKeys.Controls.Add(this.btn7);
            this.pnlBottmKeys.Controls.Add(this.btn6);
            this.pnlBottmKeys.Controls.Add(this.btn5);
            this.pnlBottmKeys.Controls.Add(this.btn4);
            this.pnlBottmKeys.Controls.Add(this.btn3);
            this.pnlBottmKeys.Controls.Add(this.btn2);
            this.pnlBottmKeys.Controls.Add(this.btn1);
            this.pnlBottmKeys.Controls.Add(this.btnEquals);
            this.pnlBottmKeys.Controls.Add(this.btnDecimal);
            this.pnlBottmKeys.Controls.Add(this.btn0);
            this.pnlBottmKeys.Controls.Add(this.btn8);
            this.pnlBottmKeys.Controls.Add(this.btn9);
            this.pnlBottmKeys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottmKeys.Location = new System.Drawing.Point(0, 157);
            this.pnlBottmKeys.Name = "pnlBottmKeys";
            this.pnlBottmKeys.Size = new System.Drawing.Size(238, 148);
            this.pnlBottmKeys.TabIndex = 20;
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn7.FlatAppearance.BorderSize = 0;
            this.btn7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn7.Location = new System.Drawing.Point(4, 6);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(41, 30);
            this.btn7.TabIndex = 11;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn6.FlatAppearance.BorderSize = 0;
            this.btn6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn6.Location = new System.Drawing.Point(98, 42);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(41, 30);
            this.btn6.TabIndex = 0;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn5.FlatAppearance.BorderSize = 0;
            this.btn5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn5.Location = new System.Drawing.Point(51, 42);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(41, 30);
            this.btn5.TabIndex = 1;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn4.FlatAppearance.BorderSize = 0;
            this.btn4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn4.Location = new System.Drawing.Point(4, 42);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(41, 30);
            this.btn4.TabIndex = 2;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn3.FlatAppearance.BorderSize = 0;
            this.btn3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn3.Location = new System.Drawing.Point(98, 78);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(41, 30);
            this.btn3.TabIndex = 3;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn2.FlatAppearance.BorderSize = 0;
            this.btn2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn2.Location = new System.Drawing.Point(51, 78);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(41, 30);
            this.btn2.TabIndex = 4;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn1.FlatAppearance.BorderSize = 0;
            this.btn1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn1.Location = new System.Drawing.Point(4, 78);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(41, 30);
            this.btn1.TabIndex = 5;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btnEquals
            // 
            this.btnEquals.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnEquals.FlatAppearance.BorderSize = 0;
            this.btnEquals.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEquals.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnEquals.Location = new System.Drawing.Point(145, 114);
            this.btnEquals.Name = "btnEquals";
            this.btnEquals.Size = new System.Drawing.Size(88, 30);
            this.btnEquals.TabIndex = 12;
            this.btnEquals.Text = "=";
            this.btnEquals.UseVisualStyleBackColor = false;
            this.btnEquals.Click += new System.EventHandler(this.btnEquals_Click);
            // 
            // btnDecimal
            // 
            this.btnDecimal.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnDecimal.FlatAppearance.BorderSize = 0;
            this.btnDecimal.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDecimal.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnDecimal.Location = new System.Drawing.Point(51, 114);
            this.btnDecimal.Name = "btnDecimal";
            this.btnDecimal.Size = new System.Drawing.Size(41, 30);
            this.btnDecimal.TabIndex = 7;
            this.btnDecimal.Text = ".";
            this.btnDecimal.UseVisualStyleBackColor = false;
            this.btnDecimal.Click += new System.EventHandler(this.btnDecimal_Click);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn0.FlatAppearance.BorderSize = 0;
            this.btn0.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn0.Location = new System.Drawing.Point(4, 114);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(41, 30);
            this.btn0.TabIndex = 8;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn8.FlatAppearance.BorderSize = 0;
            this.btn8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn8.Location = new System.Drawing.Point(51, 6);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(41, 30);
            this.btn8.TabIndex = 10;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn9.FlatAppearance.BorderSize = 0;
            this.btn9.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btn9.Location = new System.Drawing.Point(98, 6);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(41, 30);
            this.btn9.TabIndex = 9;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btnSimpleAppend_Click);
            // 
            // pnlTopKeys
            // 
            this.pnlTopKeys.BackColor = System.Drawing.Color.Transparent;
            this.pnlTopKeys.Controls.Add(this.btnStore);
            this.pnlTopKeys.Controls.Add(this.btnUp);
            this.pnlTopKeys.Controls.Add(this.btnRecall);
            this.pnlTopKeys.Controls.Add(this.btnClear);
            this.pnlTopKeys.Controls.Add(this.btnLeft);
            this.pnlTopKeys.Controls.Add(this.btnRight);
            this.pnlTopKeys.Controls.Add(this.btnDelete);
            this.pnlTopKeys.Controls.Add(this.btnDown);
            this.pnlTopKeys.Controls.Add(this.btnDRG);
            this.pnlTopKeys.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopKeys.Location = new System.Drawing.Point(0, 0);
            this.pnlTopKeys.Name = "pnlTopKeys";
            this.pnlTopKeys.Size = new System.Drawing.Size(238, 157);
            this.pnlTopKeys.TabIndex = 19;
            // 
            // btnStore
            // 
            this.btnStore.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnStore.FlatAppearance.BorderSize = 0;
            this.btnStore.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStore.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnStore.Location = new System.Drawing.Point(51, 13);
            this.btnStore.Name = "btnStore";
            this.btnStore.Size = new System.Drawing.Size(41, 23);
            this.btnStore.TabIndex = 31;
            this.btnStore.Text = "STO";
            this.btnStore.UseVisualStyleBackColor = false;
            this.btnStore.Click += new System.EventHandler(this.btnStore_Click);
            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnUp.FlatAppearance.BorderSize = 0;
            this.btnUp.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUp.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnUp.Location = new System.Drawing.Point(98, 13);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(41, 23);
            this.btnUp.TabIndex = 30;
            this.btnUp.Text = "▲";
            this.btnUp.UseVisualStyleBackColor = false;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnRecall
            // 
            this.btnRecall.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnRecall.FlatAppearance.BorderSize = 0;
            this.btnRecall.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecall.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnRecall.Location = new System.Drawing.Point(145, 13);
            this.btnRecall.Name = "btnRecall";
            this.btnRecall.Size = new System.Drawing.Size(41, 23);
            this.btnRecall.TabIndex = 29;
            this.btnRecall.Text = "RCL";
            this.btnRecall.UseVisualStyleBackColor = false;
            this.btnRecall.Click += new System.EventHandler(this.btnRecall_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnClear.Location = new System.Drawing.Point(192, 13);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(41, 23);
            this.btnClear.TabIndex = 28;
            this.btnClear.Text = "C";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnLeft.FlatAppearance.BorderSize = 0;
            this.btnLeft.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeft.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnLeft.Location = new System.Drawing.Point(51, 42);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(41, 23);
            this.btnLeft.TabIndex = 26;
            this.btnLeft.Text = "◀";
            this.btnLeft.UseVisualStyleBackColor = false;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnRight
            // 
            this.btnRight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnRight.FlatAppearance.BorderSize = 0;
            this.btnRight.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRight.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnRight.Location = new System.Drawing.Point(145, 42);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(41, 23);
            this.btnRight.TabIndex = 24;
            this.btnRight.Text = "▶";
            this.btnRight.UseVisualStyleBackColor = false;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnDelete.Location = new System.Drawing.Point(192, 42);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(41, 23);
            this.btnDelete.TabIndex = 23;
            this.btnDelete.Text = "DEL";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnDown.FlatAppearance.BorderSize = 0;
            this.btnDown.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDown.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnDown.Location = new System.Drawing.Point(98, 71);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(41, 23);
            this.btnDown.TabIndex = 20;
            this.btnDown.Text = "▼";
            this.btnDown.UseVisualStyleBackColor = false;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnDRG
            // 
            this.btnDRG.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnDRG.FlatAppearance.BorderSize = 0;
            this.btnDRG.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDRG.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnDRG.Location = new System.Drawing.Point(192, 71);
            this.btnDRG.Name = "btnDRG";
            this.btnDRG.Size = new System.Drawing.Size(41, 23);
            this.btnDRG.TabIndex = 18;
            this.btnDRG.Text = "DRG";
            this.btnDRG.UseVisualStyleBackColor = false;
            this.btnDRG.Click += new System.EventHandler(this.btnDRG_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.Lavender;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.tspToggleDisplay});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.menuStrip1.Size = new System.Drawing.Size(238, 18);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 18);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 18);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // tspToggleDisplay
            // 
            this.tspToggleDisplay.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tspToggleDisplay.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tspToggleDisplay.ForeColor = System.Drawing.Color.Black;
            this.tspToggleDisplay.Name = "tspToggleDisplay";
            this.tspToggleDisplay.Size = new System.Drawing.Size(29, 18);
            this.tspToggleDisplay.Text = "▼";
            this.tspToggleDisplay.Click += new System.EventHandler(this.tspToggleDisplay_Click);
            // 
            // txtInput
            // 
            this.txtInput.BackColor = System.Drawing.Color.AliceBlue;
            this.txtInput.Font = new System.Drawing.Font("Consolas", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInput.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtInput.Location = new System.Drawing.Point(-1, 122);
            this.txtInput.MaximumSize = new System.Drawing.Size(254, 556);
            this.txtInput.MinimumSize = new System.Drawing.Size(229, 19);
            this.txtInput.Name = "txtInput";
            this.txtInput.ReadOnly = true;
            this.txtInput.Size = new System.Drawing.Size(240, 28);
            this.txtInput.TabIndex = 0;
            this.txtInput.TabStop = false;
            this.txtInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            // 
            // txtInputBuffer
            // 
            this.txtInputBuffer.BackColor = System.Drawing.Color.AliceBlue;
            this.txtInputBuffer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInputBuffer.Font = new System.Drawing.Font("Consolas", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputBuffer.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtInputBuffer.Location = new System.Drawing.Point(-1, 145);
            this.txtInputBuffer.MaxLength = 14;
            this.txtInputBuffer.Name = "txtInputBuffer";
            this.txtInputBuffer.Size = new System.Drawing.Size(239, 32);
            this.txtInputBuffer.TabIndex = 3;
            this.txtInputBuffer.TabStop = false;
            this.txtInputBuffer.Text = "0.";
            this.txtInputBuffer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInputBuffer.TextChanged += new System.EventHandler(this.txtInputBuffer_TextChanged);
            this.txtInputBuffer.Enter += new System.EventHandler(this.txtInputBuffer_Enter);
            this.txtInputBuffer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AllControls_KeyDown);
            this.txtInputBuffer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllControls_KeyPress);
            // 
            // pnlFrontPanel
            // 
            this.pnlFrontPanel.BackColor = System.Drawing.Color.Lavender;
            this.pnlFrontPanel.Controls.Add(this.pnlKeys);
            this.pnlFrontPanel.Controls.Add(this.pnlDisplay);
            this.pnlFrontPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFrontPanel.Location = new System.Drawing.Point(0, 18);
            this.pnlFrontPanel.Name = "pnlFrontPanel";
            this.pnlFrontPanel.Size = new System.Drawing.Size(238, 493);
            this.pnlFrontPanel.TabIndex = 4;
            // 
            // pnlDisplay
            // 
            this.pnlDisplay.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pnlDisplay.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlDisplay.Controls.Add(this.expressionHistoryLabel5);
            this.pnlDisplay.Controls.Add(this.expressionHistoryLabel4);
            this.pnlDisplay.Controls.Add(this.expressionHistoryLabel3);
            this.pnlDisplay.Controls.Add(this.expressionHistoryLabel2);
            this.pnlDisplay.Controls.Add(this.expressionHistoryLabel1);
            this.pnlDisplay.Controls.Add(this.lblMemory);
            this.pnlDisplay.Controls.Add(this.lblShift);
            this.pnlDisplay.Controls.Add(this.lblDRG);
            this.pnlDisplay.Controls.Add(this.txtInputBuffer);
            this.pnlDisplay.Controls.Add(this.txtInput);
            this.pnlDisplay.Location = new System.Drawing.Point(0, 1);
            this.pnlDisplay.Name = "pnlDisplay";
            this.pnlDisplay.Size = new System.Drawing.Size(238, 204);
            this.pnlDisplay.TabIndex = 3;
            // 
            // expressionHistoryLabel5
            // 
            this.expressionHistoryLabel5.Expression = null;
            this.expressionHistoryLabel5.Font = new System.Drawing.Font("Consolas", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expressionHistoryLabel5.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.expressionHistoryLabel5.Location = new System.Drawing.Point(4, 7);
            this.expressionHistoryLabel5.Name = "expressionHistoryLabel5";
            this.expressionHistoryLabel5.Size = new System.Drawing.Size(229, 19);
            this.expressionHistoryLabel5.TabIndex = 11;
            this.expressionHistoryLabel5.Text = "expressionHistoryLabel5";
            this.expressionHistoryLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.expressionHistoryLabel5.Click += new System.EventHandler(this.expressionHistoryLabel_Click);
            // 
            // expressionHistoryLabel4
            // 
            this.expressionHistoryLabel4.Expression = null;
            this.expressionHistoryLabel4.Font = new System.Drawing.Font("Consolas", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expressionHistoryLabel4.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.expressionHistoryLabel4.Location = new System.Drawing.Point(4, 30);
            this.expressionHistoryLabel4.Name = "expressionHistoryLabel4";
            this.expressionHistoryLabel4.Size = new System.Drawing.Size(229, 19);
            this.expressionHistoryLabel4.TabIndex = 10;
            this.expressionHistoryLabel4.Text = "expressionHistoryLabel4";
            this.expressionHistoryLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.expressionHistoryLabel4.Click += new System.EventHandler(this.expressionHistoryLabel_Click);
            // 
            // expressionHistoryLabel3
            // 
            this.expressionHistoryLabel3.Expression = null;
            this.expressionHistoryLabel3.Font = new System.Drawing.Font("Consolas", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expressionHistoryLabel3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.expressionHistoryLabel3.Location = new System.Drawing.Point(4, 53);
            this.expressionHistoryLabel3.Name = "expressionHistoryLabel3";
            this.expressionHistoryLabel3.Size = new System.Drawing.Size(229, 19);
            this.expressionHistoryLabel3.TabIndex = 9;
            this.expressionHistoryLabel3.Text = "expressionHistoryLabel3";
            this.expressionHistoryLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.expressionHistoryLabel3.Click += new System.EventHandler(this.expressionHistoryLabel_Click);
            // 
            // expressionHistoryLabel2
            // 
            this.expressionHistoryLabel2.Expression = null;
            this.expressionHistoryLabel2.Font = new System.Drawing.Font("Consolas", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expressionHistoryLabel2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.expressionHistoryLabel2.Location = new System.Drawing.Point(4, 75);
            this.expressionHistoryLabel2.Name = "expressionHistoryLabel2";
            this.expressionHistoryLabel2.Size = new System.Drawing.Size(229, 19);
            this.expressionHistoryLabel2.TabIndex = 8;
            this.expressionHistoryLabel2.Text = "expressionHistoryLabel2";
            this.expressionHistoryLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.expressionHistoryLabel2.Click += new System.EventHandler(this.expressionHistoryLabel_Click);
            // 
            // expressionHistoryLabel1
            // 
            this.expressionHistoryLabel1.Expression = null;
            this.expressionHistoryLabel1.Font = new System.Drawing.Font("Consolas", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expressionHistoryLabel1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.expressionHistoryLabel1.Location = new System.Drawing.Point(4, 97);
            this.expressionHistoryLabel1.Name = "expressionHistoryLabel1";
            this.expressionHistoryLabel1.Size = new System.Drawing.Size(229, 19);
            this.expressionHistoryLabel1.TabIndex = 7;
            this.expressionHistoryLabel1.Text = "expressionHistoryLabel1";
            this.expressionHistoryLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.expressionHistoryLabel1.Click += new System.EventHandler(this.expressionHistoryLabel_Click);
            // 
            // lblMemory
            // 
            this.lblMemory.AutoSize = true;
            this.lblMemory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMemory.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemory.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblMemory.Location = new System.Drawing.Point(66, 175);
            this.lblMemory.Name = "lblMemory";
            this.lblMemory.Size = new System.Drawing.Size(14, 11);
            this.lblMemory.TabIndex = 6;
            this.lblMemory.Text = "M";
            this.lblMemory.Visible = false;
            // 
            // lblShift
            // 
            this.lblShift.AutoSize = true;
            this.lblShift.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShift.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblShift.Location = new System.Drawing.Point(4, 175);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(30, 11);
            this.lblShift.TabIndex = 5;
            this.lblShift.Text = "SHIFT";
            this.lblShift.Visible = false;
            // 
            // lblDRG
            // 
            this.lblDRG.AutoSize = true;
            this.lblDRG.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDRG.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblDRG.Location = new System.Drawing.Point(37, 175);
            this.lblDRG.Name = "lblDRG";
            this.lblDRG.Size = new System.Drawing.Size(24, 11);
            this.lblDRG.TabIndex = 4;
            this.lblDRG.Text = "DEG";
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnEquals;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClear;
            this.ClientSize = new System.Drawing.Size(238, 511);
            this.Controls.Add(this.pnlFrontPanel);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(254, 550);
            this.MinimumSize = new System.Drawing.Size(254, 425);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DAL Calculator";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AllControls_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllControls_KeyPress);
            this.pnlKeys.ResumeLayout(false);
            this.pnlBottmKeys.ResumeLayout(false);
            this.pnlTopKeys.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlFrontPanel.ResumeLayout(false);
            this.pnlDisplay.ResumeLayout(false);
            this.pnlDisplay.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlKeys;
        private System.Windows.Forms.Panel pnlTopKeys;
        private System.Windows.Forms.Panel pnlBottmKeys;
        private DefaultStyleButton btn6;
        private DefaultStyleButton btn7;
        private DefaultStyleButton btn5;
        private DefaultStyleButton btn4;
        private DefaultStyleButton btn3;
        private DefaultStyleButton btn2;
        private DefaultStyleButton btn1;
        private DefaultStyleButton btnEquals;
        private DefaultStyleButton btnDecimal;
        private DefaultStyleButton btn0;
        private DefaultStyleButton btn8;
        private DefaultStyleButton btn9;
        private DefaultStyleButton btnStore;
        private DefaultStyleButton btnUp;
        private DefaultStyleButton btnRecall;
        private DefaultStyleButton btnClear;
        private DefaultStyleButton btnLeft;
        private DefaultStyleButton btnRight;
        private DefaultStyleButton btnDelete;
        private DefaultStyleButton btnDown;
        private DefaultStyleButton btnDRG;
        private DefaultStyleButton btnMemoryPlus;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TextBox txtInputBuffer;
        private System.Windows.Forms.Panel pnlFrontPanel;
        private System.Windows.Forms.Panel pnlDisplay;
        private System.Windows.Forms.Label lblMemory;
        private System.Windows.Forms.Label lblShift;
        private System.Windows.Forms.Label lblDRG;
        private ExpressionHistoryLabel expressionHistoryLabel5;
        private ExpressionHistoryLabel expressionHistoryLabel4;
        private ExpressionHistoryLabel expressionHistoryLabel3;
        private ExpressionHistoryLabel expressionHistoryLabel2;
        private ExpressionHistoryLabel expressionHistoryLabel1;
        private System.Windows.Forms.ToolStripMenuItem tspToggleDisplay;
    }
}

