﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScientificCalculator
{
    class ConstantButton : DefaultStyleButton
    {
        public double Value { get; private set; }

        public ConstantButton(double value) : base()
        {
            Value = value;
        }
    }
}
