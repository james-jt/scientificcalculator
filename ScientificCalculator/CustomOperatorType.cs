﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificCalculator
{
    public enum PrecedenceLevel
    {
        VeryLow,
        Low,
        Normal,
        High,
        VeryHigh
    }

    public class CustomOperatorType 
    {
        public CustomOperators.OperatorNames OperatorName { get; private set; }
        public char OperatorCode { get; private set; }
        public string OperatorCaption { get; private set; }
        public string OperatorSymbol { get; private set; }
        public bool IsBinaryOperator { get; private set; }

        public Func<double[], double> OperatorFunction;
        public CustomOperatorType(CustomOperators.OperatorNames Name, bool isBinary)
        {
            OperatorName = Name;
            OperatorCode = CustomOperators.GetCustomOperatorCode(OperatorName);
            OperatorCaption = CustomOperators.GetCustomOperatorCaption(OperatorName);
            OperatorSymbol = CustomOperators.GetCustomOperatorSymbol(OperatorName);
            OperatorFunction = CustomOperators.GetCustomOperatorFunction(OperatorName);
            IsBinaryOperator = isBinary;
        }
    }
}
 