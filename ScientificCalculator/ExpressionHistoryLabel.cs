﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScientificCalculator
{
    public class ExpressionHistoryLabel : Label
    {
        public ExpressionType Expression { get; set; }

        public ExpressionHistoryLabel() : base() { }
    }
}
