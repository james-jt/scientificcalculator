﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ScientificCalculator.CustomOperators;

namespace ScientificCalculator
{
    public static class CustomOperatorsExtensionMethods
    {
        public static OperatorNames GetCustomOperatorName(this CustomOperators customOperators, char op)
        {
            foreach (KeyValuePair<OperatorNames, char> pair in customOperators.OperatorCodes)
            {
                if (pair.Value == op)
                {
                    return pair.Key;
                }
            }
            return OperatorNames.Unknown;
        }
        public static CustomOperatorType GetCustomOperatorType(this CustomOperators customOperators, char op)
        {
            OperatorNames opName = GetCustomOperatorName(customOperators, op);
            CustomOperatorType customOperatorType = default(CustomOperatorType);
            foreach (CustomOperatorType customOperator in customOperators.AllCustomOperators)
            {
                if (opName == customOperator.OperatorName)
                {
                    customOperatorType = customOperator;
                    break;
                }
            }
            if (customOperatorType == default(CustomOperatorType))
            {
                throw new Exception("Unknown operator found.");
            }
            else
            {
                return customOperatorType;
            }
        }
        public static char GetCustomOperatorCode(this CustomOperators customOperators, CustomOperatorType opType)
        {
            char opCode = default(char);
            foreach (KeyValuePair<OperatorNames, char> pair in customOperators.OperatorCodes)
            {
                if (pair.Key == opType.OperatorName)
                {
                    opCode = pair.Value;
                    break;
                }
            }
            if (opCode == default(char))
            {
                throw new Exception("Operator code is not defined.");
            }
            else
            {
                return opCode;
            }
        }
        public static char GetCustomOperatorCode(this CustomOperators customOperators, OperatorNames opName)
        {
            char opCode = default(char);
            foreach (KeyValuePair<OperatorNames, char> pair in customOperators.OperatorCodes)
            {
                if (pair.Key == opName)
                {
                    opCode = pair.Value;
                    break;
                }
            }
            if (opCode == default(char))
            {
                throw new Exception("Operator code is not defined.");
            }
            else
            {
                return opCode;
            }
        }
        public static string GetCustomOperatorCaption(this CustomOperators customOperators, OperatorNames opName)
        {
            string opCaption = string.Empty;
            foreach (KeyValuePair<OperatorNames, string> pair in customOperators.OperatorCaptions)
            {
                if (pair.Key == opName)
                {
                    opCaption = pair.Value;
                    break;
                }
            }
            if (string.IsNullOrWhiteSpace(opCaption))
            {
                throw new Exception("Operator caption is not defined.");
            }
            else
            {
                return opCaption;
            }
        }
        public static string GetCustomOperatorSymbol(this CustomOperators customOperators, OperatorNames opName)
        {
            string opSymbol = string.Empty;
            foreach (KeyValuePair<OperatorNames, string> pair in customOperators.OperatorSymbols)
            {
                if (pair.Key == opName)
                {
                    opSymbol = pair.Value;
                    break;
                }
            }
            if (string.IsNullOrWhiteSpace(opSymbol))
            {
                throw new Exception("Operator symbol is not defined.");
            }
            else
            {
                return opSymbol;
            }
        }
        public static string GetCustomOperatorSymbol(this CustomOperators customOperators, CustomOperatorType opType)
        {
            string opSymbol = string.Empty;
            foreach (KeyValuePair<OperatorNames, string> pair in customOperators.OperatorCaptions)
            {
                if (pair.Key == opType.OperatorName)
                {
                    opSymbol = pair.Value;
                    break;
                }
            }
            if (string.IsNullOrWhiteSpace(opSymbol))
            {
                throw new Exception("Operator symbol is not defined.");
            }
            else
            {
                return opSymbol;
            }
        }
        public static OperatorFunctionDelegate GetCustomOperatorFunction(this CustomOperators customOperators, OperatorNames opName)
        {
            OperatorFunctionDelegate opFunction = default(OperatorFunctionDelegate);
            foreach (KeyValuePair<OperatorNames, OperatorFunctionDelegate> pair in customOperators.OperatorFunctions)
            {
                if (pair.Key == opName)
                {
                    opFunction = pair.Value;
                    break;
                }
            }
            if (opFunction == default(OperatorFunctionDelegate))
            {
                throw new Exception("Operator symbol is not defined.");
            }
            else
            {
                return opFunction;
            }
        }

    }
}
