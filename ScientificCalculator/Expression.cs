﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificCalculator
{
    public class ExpressionType : IComparable
    { 
        private static int ExpressionsCounter = 0;
        public string DisplayText { get; set; }
        public string EvaluationText { get; set; }
        public double Result { get; set; }

        public int Id { get; private set; }
        public ExpressionType()
        {
            Id = ++ExpressionsCounter;
        }

        public int CompareTo(object obj)
        {
            if(obj is ExpressionType)
            {
                ExpressionType otherExpression = obj as ExpressionType;
                return this.Id.CompareTo(otherExpression.Id);
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
