﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientificCalculator
{
    public class CustomOperators
    {
        public enum OperatorNames
        {
            Addition,
            Subtraction,
            Multiplication,
            Division,
            Negation,
            SRoot,
            CubeRoot,
            Square,
            Cube,
            Power,
            NRoot,
            NaturalLog,
            NaturalExp,
            Base10Log,
            Exponent,
            Factorial,
            Sine,
            arcSine,
            Cosine,
            arcCosine,
            Tangent,
            arcTangent,
            Inverse,
            OpenBracket,
            CloseBracket,
            Percentage
            //Unknown
        }

        public enum DRG_Mode
        {
            Degrees,
            Radians,
            Gradians
        }

        private static Dictionary<OperatorNames, char> OperatorCodes { get; set; }
        private static Dictionary<OperatorNames, string> OperatorCaptions { get; set; }
        private static Dictionary<OperatorNames, string> OperatorSymbols { get; set; }
        private static Dictionary<OperatorNames, Func<double[], double>> OperatorFunctions { get;  set; }

        private static List<CustomOperatorType> AllCustomOperators { get; set; }
        public static List<CustomOperatorType> PostOperandOperators { get; private set; }

        public CustomOperatorType DivisionOperator { get; private set; }
        public CustomOperatorType MultiplicationOperator { get; private set; }
        public CustomOperatorType AdditionOperator { get; private set; }
        public CustomOperatorType SubtractionOperator { get; private set; }
        public CustomOperatorType NegationOperator { get; private set; }
        public CustomOperatorType SineOperator { get; private set; }
        public CustomOperatorType arcSineOperator { get; private set; }
        public CustomOperatorType CosineOperator { get; private set; }
        public CustomOperatorType arcCosineOperator { get; private set; }
        public CustomOperatorType TangentOperator { get; private set; }
        public CustomOperatorType arcTangentOperator { get; private set; }
        public CustomOperatorType NaturalLogOperator { get; private set; }
        public CustomOperatorType NaturalExpOperator { get; private set; }
        public CustomOperatorType Base10LogOperator { get; private set; }
        public CustomOperatorType ExponentOperator { get; private set; }
        public CustomOperatorType SRootOperator { get; private set; }
        public CustomOperatorType CubeRootOperator { get; private set; }
        public CustomOperatorType NRootOperator { get; private set; }
        public CustomOperatorType SquareOperator { get; private set; }
        public CustomOperatorType CubeOperator { get; private set; }
        public CustomOperatorType InverseOperator { get; private set; }
        public CustomOperatorType PowerOperator { get; private set; }
        public CustomOperatorType FactorialOperator { get; private set; }
        public CustomOperatorType OpenBracketOperator { get; private set; }
        public CustomOperatorType CloseBracketOperator { get; private set; }
        public CustomOperatorType PercentageOperator { get; private set; }

        #region OperatorFunctions
         
        public static DRG_Mode currentDRG_Mode = DRG_Mode.Degrees;

        private static double AdditionFunction (double[] args) => args[1] + args[0];
        private static double SubtractionFunction (double[] args) => args[1] - args[0];
        private static double MultiplicationFunction (double[] args) => args[1] * args[0];
        private static double DivisionFunction (double[] args)
        {
            if (args[0] == 0)
            {
                throw new DivideByZeroException();
            }
            return args[1] / args[0];
        }
        private static double NegationFunction (double[] args) => args[0] * -1;       
        private static double SRootFunction (double[] args) => Math.Sqrt(args[0]); //Confirm if negative numbers can be automatically converted to complex numbers by the .NET function
        private static double CubeRootFunction (double[] args) => Math.Pow(args[0], DivisionFunction(new [] { 3, 1.0 }));
        private static double SquareFunction (double[] args) => args[0] * args[0];
        private static double CubeFunction (double[] args) => Math.Pow(args[0], 3);
        private static double PowerFunction (double[] args) => Math.Pow(args[1], args[0]);
        private static double NRootFunction (double[] args) => Math.Pow(args[0], DivisionFunction(new[] { args[1], 1 }));
        private static double NaturalLogFunction (double[] args) => Math.Log(args[0]);
        private static double NaturalExpFunction (double[] args) => Math.Exp(args[0]);
        private static double Base10LogFunction (double[] args) => Math.Log10(args[0]);
        private static double ExponentFunction (double[] args) => Math.Pow(10, args[0]);
        private static double FactorialFunction (double[] args) => (args[0] < 2)? 1 : args[0] * FactorialFunction(new[] { args[0] - 1 });
        private static double SineFunction (double[] args)
        {
            double angle;
            if(currentDRG_Mode == DRG_Mode.Degrees)
            {
                angle = DegreesToRadians(args[0]);
            }
            else if(currentDRG_Mode == DRG_Mode.Radians)
            {
                angle = args[0];
            }
            else
            {
                angle = GradiansToRadians(args[0]);
            }
            return Math.Sin(angle);
        }
        private static double arcSineFunction (double[] args)
        {
            double value;
            if (currentDRG_Mode == DRG_Mode.Degrees)
            {
                value = DegreesToRadians(args[0]);
            }
            else if (currentDRG_Mode == DRG_Mode.Radians)
            {
                value = args[0];
            }
            else
            {
                value = GradiansToRadians(args[0]);
            }
            return Math.Asin(value);
        }
        private static double CosineFunction (double[] args)
        {
            double angle;
            if (currentDRG_Mode == DRG_Mode.Degrees)
            {
                angle = DegreesToRadians(args[0]);
            }
            else if (currentDRG_Mode == DRG_Mode.Radians)
            {
                angle = args[0];
            }
            else
            {
                angle = GradiansToRadians(args[0]);
            }
            return Math.Cos(angle);
        }
        private static double arcCosineFunction (double[] args)
        {
            double value;
            if (currentDRG_Mode == DRG_Mode.Degrees)
            {
                value = DegreesToRadians(args[0]);
            }
            else if (currentDRG_Mode == DRG_Mode.Radians)
            {
                value = args[0];
            }
            else
            {
                value = GradiansToRadians(args[0]);
            }
            return Math.Acos(value);
        }
        private static double TangentFunction(double[] args)
        {
            double angle;
            if (currentDRG_Mode == DRG_Mode.Degrees)
            {
                angle = DegreesToRadians(args[0]);
            }
            else if (currentDRG_Mode == DRG_Mode.Radians)
            {
                angle = args[0];
            }
            else
            {
                angle = GradiansToRadians(args[0]);
            }
            return Math.Tan(angle);
        }
        private static double arcTangentFunction (double[] args)
        {
            double value;
            if (currentDRG_Mode == DRG_Mode.Degrees)
            {
                value = DegreesToRadians(args[0]);
            }
            else if (currentDRG_Mode == DRG_Mode.Radians)
            {
                value = args[0];
            }
            else
            {
                value = GradiansToRadians(args[0]);
            }
            return Math.Atan(value);
        }
        private static double InverseFunction (double[] args) => Math.Pow(args[0], -1);
        private static double OpenBracketFunction (double[] args) => 0;
        private static double CloseBracketFunction(double[] args) => 0;
        public static double PercentageFunction (double[] args) => args[0] / 100;

        #region Common Functions

        private static double DegreesToRadians(double valueInDegrees) => valueInDegrees * Math.PI / 180;
        private static double GradiansToRadians(double valueInGradians) => valueInGradians * Math.PI / 200;

        #endregion Common Functions

        #endregion OperatorFunctions

        //Implement the Singleton pattern to enforce the same instance of this class throughout app.
        public CustomOperators()
        { 
            #region OperatorCodes Dictionary

            OperatorCodes = new Dictionary<OperatorNames, char>()
            {
                {OperatorNames.Addition, 'A' },
                {OperatorNames.Subtraction, 'B'},

                {OperatorNames.Multiplication, 'C'},
                {OperatorNames.Division, 'D'},

                {OperatorNames.Sine, 'E'},
                {OperatorNames.arcSine, 'F'},
                {OperatorNames.Cosine, 'G'},
                {OperatorNames.arcCosine, 'H'},
                {OperatorNames.Tangent, 'I'},
                {OperatorNames.arcTangent, 'J'},

                {OperatorNames.Negation, 'K'},

                {OperatorNames.SRoot, 'L'},
                {OperatorNames.CubeRoot, 'M'},
                {OperatorNames.Square, 'N'},
                {OperatorNames.Cube, 'O'},
                {OperatorNames.Power, 'P'},
                {OperatorNames.NRoot, 'Q'},
                {OperatorNames.NaturalLog, 'R'},
                {OperatorNames.NaturalExp, 'S'},
                {OperatorNames.Base10Log, 'T'},
                {OperatorNames.Exponent, 'U'},
                {OperatorNames.Inverse, 'V'},

                {OperatorNames.Percentage, 'W' },
                {OperatorNames.Factorial, 'X'},

                {OperatorNames.OpenBracket, '('},
                {OperatorNames.CloseBracket, ')'}
            };

        #endregion OperatorCodes Dictionary

            #region OperatorCaptions Dictionary

            OperatorCaptions = new Dictionary<OperatorNames, string>()
                {
                    {OperatorNames.Addition, "+" },
                    {OperatorNames.Subtraction, "-"},
                    {OperatorNames.Multiplication, "×"},
                    {OperatorNames.Division, "÷"},
                    {OperatorNames.Negation, "-"},
                    {OperatorNames.SRoot, "√"},
                    {OperatorNames.CubeRoot, "³√"},
                    {OperatorNames.Square, "x²"},
                    {OperatorNames.Cube, "x³"},
                    {OperatorNames.Power, "xʸ"},
                    {OperatorNames.NRoot, "ʸ√"},
                    {OperatorNames.NaturalLog, "ln"},
                    {OperatorNames.NaturalExp, "eʸ"},
                    {OperatorNames.Base10Log, "log"},
                    {OperatorNames.Exponent, "10ʸ"},
                    {OperatorNames.Factorial, "n!"},
                    {OperatorNames.Sine, "sin"},
                    {OperatorNames.arcSine, "sin⁻¹"},
                    {OperatorNames.Cosine, "cos"},
                    {OperatorNames.arcCosine, "cos⁻¹"},
                    {OperatorNames.Tangent, "tan"},
                    {OperatorNames.arcTangent, "tan⁻¹"},
                    {OperatorNames.Inverse, "x⁻¹"},
                    {OperatorNames.OpenBracket, "("},
                    {OperatorNames.CloseBracket, ")"},
                    {OperatorNames.Percentage, "%" }
                };

            #endregion OperatorSymbols Dictionary

            #region OperatorSymbols Dictionary

            OperatorSymbols = new Dictionary<OperatorNames, string>()
                {
                    {OperatorNames.Addition, " + " },
                    {OperatorNames.Subtraction, " - "},
                    {OperatorNames.Multiplication, " × "},
                    {OperatorNames.Division, " ÷ "},
                    {OperatorNames.Negation, "-"},
                    {OperatorNames.SRoot, "√"},
                    {OperatorNames.CubeRoot, "³√"},
                    {OperatorNames.Square, "²"},
                    {OperatorNames.Cube, "^³"},
                    {OperatorNames.Power, "^"},
                    {OperatorNames.NRoot, "ʸ√"},
                    {OperatorNames.NaturalLog, "ln"},
                    {OperatorNames.NaturalExp, "e^"},
                    {OperatorNames.Base10Log, "log"},
                    {OperatorNames.Exponent, "10^"},
                    {OperatorNames.Factorial, "!"},
                    {OperatorNames.Sine, "sin"},
                    {OperatorNames.arcSine, "sin⁻¹"},
                    {OperatorNames.Cosine, "cos"},
                    {OperatorNames.arcCosine, "cos⁻¹"},
                    {OperatorNames.Tangent, "tan"},
                    {OperatorNames.arcTangent, "tan⁻¹"},
                    {OperatorNames.Inverse, "⁻¹"},
                    {OperatorNames.OpenBracket, "("},
                    {OperatorNames.CloseBracket, ")"},
                    {OperatorNames.Percentage, "%" }
                };

            #endregion OperatorSymbols Dictionary

            #region OperatorFunctions Dictionary

        OperatorFunctions = new Dictionary<OperatorNames, Func<double[], double>>()
            {
                {OperatorNames.Addition, AdditionFunction},
                {OperatorNames.Subtraction, SubtractionFunction},

                {OperatorNames.Multiplication, MultiplicationFunction},
                {OperatorNames.Division, DivisionFunction},

                {OperatorNames.Sine, SineFunction},
                {OperatorNames.arcSine, arcSineFunction},
                {OperatorNames.Cosine, CosineFunction},
                {OperatorNames.arcCosine, arcCosineFunction},
                {OperatorNames.Tangent, TangentFunction},
                {OperatorNames.arcTangent, arcTangentFunction},

                {OperatorNames.Negation, NegationFunction},

                {OperatorNames.SRoot, SRootFunction},
                {OperatorNames.CubeRoot, CubeRootFunction},
                {OperatorNames.Square, SquareFunction},
                {OperatorNames.Cube, CubeFunction},
                {OperatorNames.Power, PowerFunction},
                {OperatorNames.NRoot, NRootFunction},
                {OperatorNames.NaturalLog, NaturalLogFunction},
                {OperatorNames.NaturalExp, NaturalExpFunction},
                {OperatorNames.Base10Log, Base10LogFunction},
                {OperatorNames.Exponent, ExponentFunction},
                {OperatorNames.Inverse, InverseFunction},
                {OperatorNames.Percentage, PercentageFunction },
                {OperatorNames.Factorial, FactorialFunction},

                {OperatorNames.OpenBracket, OpenBracketFunction},
                {OperatorNames.CloseBracket, CloseBracketFunction},
            };

            #endregion OperatorFunctions Dictionary

            #region Operator Objects

            AllCustomOperators = new List<CustomOperatorType>();

            DivisionOperator = new CustomOperatorType(OperatorNames.Division, true);
            AllCustomOperators.Add(DivisionOperator);

            MultiplicationOperator = new CustomOperatorType(OperatorNames.Multiplication, true);
            AllCustomOperators.Add(MultiplicationOperator);

            AdditionOperator = new CustomOperatorType(OperatorNames.Addition, true);
            AllCustomOperators.Add(AdditionOperator);

            SubtractionOperator = new CustomOperatorType(OperatorNames.Subtraction, true);
            AllCustomOperators.Add(SubtractionOperator);

            NegationOperator = new CustomOperatorType(OperatorNames.Negation, false);
            AllCustomOperators.Add(NegationOperator);

            SineOperator = new CustomOperatorType(OperatorNames.Sine, false);
            AllCustomOperators.Add(SineOperator);

            arcSineOperator = new CustomOperatorType(OperatorNames.arcSine, false);
            AllCustomOperators.Add(arcSineOperator);

            CosineOperator = new CustomOperatorType(OperatorNames.Cosine, false);
            AllCustomOperators.Add(CosineOperator);

            arcCosineOperator = new CustomOperatorType(OperatorNames.arcCosine, false);
            AllCustomOperators.Add(arcCosineOperator);

            TangentOperator = new CustomOperatorType(OperatorNames.Tangent, false);
            AllCustomOperators.Add(TangentOperator);

            arcTangentOperator = new CustomOperatorType(OperatorNames.arcTangent, false);
            AllCustomOperators.Add(arcTangentOperator);

            NaturalLogOperator = new CustomOperatorType(OperatorNames.NaturalLog, false);
            AllCustomOperators.Add(NaturalLogOperator);

            NaturalExpOperator = new CustomOperatorType(OperatorNames.NaturalExp, false);
            AllCustomOperators.Add(NaturalExpOperator);

            Base10LogOperator = new CustomOperatorType(OperatorNames.Base10Log, false);
            AllCustomOperators.Add(Base10LogOperator);

            ExponentOperator = new CustomOperatorType(OperatorNames.Exponent, false);
            AllCustomOperators.Add(ExponentOperator);

            SRootOperator = new CustomOperatorType(OperatorNames.SRoot, false);
            AllCustomOperators.Add(SRootOperator);

            CubeRootOperator = new CustomOperatorType(OperatorNames.CubeRoot, false);
            AllCustomOperators.Add(CubeRootOperator);

            NRootOperator = new CustomOperatorType(OperatorNames.NRoot, true);
            AllCustomOperators.Add(NRootOperator);

            SquareOperator = new CustomOperatorType(OperatorNames.Square, false);
            AllCustomOperators.Add(SquareOperator);

            CubeOperator = new CustomOperatorType(OperatorNames.Cube, false);
            AllCustomOperators.Add(CubeOperator);

            InverseOperator = new CustomOperatorType(OperatorNames.Inverse, false);
            AllCustomOperators.Add(InverseOperator);

            PowerOperator = new CustomOperatorType(OperatorNames.Power, true);
            AllCustomOperators.Add(PowerOperator);

            FactorialOperator = new CustomOperatorType(OperatorNames.Factorial, false);
            AllCustomOperators.Add(FactorialOperator);

            PercentageOperator = new CustomOperatorType(OperatorNames.Percentage, false);
            AllCustomOperators.Add(PercentageOperator);

            OpenBracketOperator = new CustomOperatorType(OperatorNames.OpenBracket, false);
            AllCustomOperators.Add(OpenBracketOperator);

            CloseBracketOperator = new CustomOperatorType(OperatorNames.CloseBracket, false);
            AllCustomOperators.Add(CloseBracketOperator);

            PostOperandOperators = new List<CustomOperatorType>();
            PostOperandOperators.Add(InverseOperator);
            PostOperandOperators.Add(SquareOperator);
            PostOperandOperators.Add(CubeOperator);
            PostOperandOperators.Add(FactorialOperator);
            PostOperandOperators.Add(PercentageOperator);
            PostOperandOperators.Add(AdditionOperator);
            PostOperandOperators.Add(SubtractionOperator);
            PostOperandOperators.Add(MultiplicationOperator);
            PostOperandOperators.Add(DivisionOperator);
            PostOperandOperators.Add(PowerOperator);
            PostOperandOperators.Add(NRootOperator);

            #endregion Operator Objects
        }

        public CustomOperatorType this[int index]
        {
            get { return CustomOperators.AllCustomOperators[index]; }
            set { CustomOperators.AllCustomOperators[index] = value; }
        }

        #region Static Utility Methods

        public static OperatorNames GetCustomOperatorName(char op)
        {
            foreach (KeyValuePair<OperatorNames, char> pair in OperatorCodes)
            {
                if (pair.Value == op)
                {
                    return pair.Key;
                }
            }
            throw new KeyNotFoundException("Unknown Operator Name.");
        }

        public static CustomOperatorType GetCustomOperatorType(char op)
        {
            OperatorNames opName;
            try
            {
                opName = GetCustomOperatorName(op);
            }
            catch (KeyNotFoundException)
            {
                throw;
            }
            CustomOperatorType customOperatorType = default(CustomOperatorType);
            foreach (CustomOperatorType customOperator in AllCustomOperators)
            {
                if (opName == customOperator.OperatorName)
                {
                    customOperatorType = customOperator;
                    break;
                }
            }
            if (customOperatorType == default(CustomOperatorType))
            {
                throw new Exception("Unknown operator found.");
            }
            else
            {
                return customOperatorType;
            }
        }

        public static char GetCustomOperatorCode(CustomOperatorType opType)
        {
            try
            {
                return OperatorCodes[opType.OperatorName];
            }
            catch(KeyNotFoundException e)
            {
                throw new Exception("Operator code is not defined.");
            }
        }

        public static char GetCustomOperatorCode(OperatorNames opName)
        {
            try
            {
                return OperatorCodes[opName];
            }
            catch (KeyNotFoundException e)
            {
                throw new Exception("Operator code is not defined.");
            }
        }

        public static string GetCustomOperatorCaption(OperatorNames opName)
        {
            try
            {
                return OperatorCaptions[opName];
            }
            catch (KeyNotFoundException e)
            {
                throw new Exception("Operator caption is not defined.");
            }
        }
        public static string GetCustomOperatorSymbol(OperatorNames opName)
        {
            try
            {
                return OperatorSymbols[opName];
            }
            catch (KeyNotFoundException e)
            {
                throw new Exception("Operator symbol is not defined.");
            }
        }
        public static string GetCustomOperatorSymbol(CustomOperatorType opType)
        {
            try
            {
                return OperatorSymbols[opType.OperatorName];
            }
            catch (KeyNotFoundException e)
            {
                throw new Exception("Operator symbol is not defined.");
            }
        }

        public static Func<double[], double> GetCustomOperatorFunction(OperatorNames opName)
        {
            try
            {
                return OperatorFunctions[opName];
            }
            catch (KeyNotFoundException e)
            {
                throw new Exception("Operator function is not defined.");
            }
        }

        #endregion Static Utility Methods

    }
}
