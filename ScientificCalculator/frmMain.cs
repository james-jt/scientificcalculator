﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace ScientificCalculator
{
    public partial class frmMain : Form
    {
        public delegate void TestDelegate<T>();
        private List<OperatorButton> OperatorButtons;
        public StringBuilder inputBufferString;
        public StringBuilder inputString;
        CustomOperators customOperators;
        private List<ExpressionType> ExpressionsHistoryEntries;
        private List<ExpressionHistoryLabel> ExpressionsHistoryLabels;
        int currentExpressionsHistoryItem;
        int currentExpressionHistoryPage;
        int cursor;
        const int NO_OF_EXPRESSION_HISTORY_DISPLAY_ITEMS = 5;
        private KeyType keyType;
        private enum KeyType { Normal, Operator, Constant, Unknown }

        public frmMain()
        {
            OperatorButtons = new List<OperatorButton>();
            inputBufferString = new StringBuilder();
            inputString = new StringBuilder();
            customOperators = new CustomOperators();

            InitializeComponent();
            InitializeCustomComponent();
            this.MouseWheel += frmMain_MouseWheel;

            ExpressionsHistoryEntries = new List<ExpressionType>();
            ExpressionsHistoryLabels = new List<ExpressionHistoryLabel>() { expressionHistoryLabel1, expressionHistoryLabel2, expressionHistoryLabel3, expressionHistoryLabel4, expressionHistoryLabel5 };
            foreach (Label label in ExpressionsHistoryLabels)
            {
                label.Text = string.Empty;
            }
            currentExpressionsHistoryItem = 0;
            currentExpressionHistoryPage = 0;
            cursor = 0;

            Logic.IsMemoryEngaged = false;
        }

        private void frmMain_MouseWheel(object sender, MouseEventArgs e)
        {
            double numberOfOpacityUnitsToMove = (e.Delta / 120.0)/10;

            if (this.Opacity >= 0.3 && this.Opacity <= 1)
            {
                this.Opacity += numberOfOpacityUnitsToMove;
                this.Opacity = (this.Opacity < 0.3) ? 0.4 : this.Opacity;
                this.Opacity = (this.Opacity > 1) ? 1 : this.Opacity;

            }
            this.Invalidate();
        }

        #region Helper Methods

        private void ToggleShift()
        {
            btnShift.IsSet = !btnShift.IsSet;
            if (btnShift.IsSet)
            {
                btnShift.BackColor = Color.AliceBlue;
                lblShift.Visible = true;
            }
            else
            {
                btnShift.BackColor = SystemColors.GradientInactiveCaption;
                lblShift.Visible = false;
            }
            btnShift.OnShiftToggled();
        }

        //Needs some work for improved user experience
        private void AppendToInputString(object sender)
        { 
            StringBuilder tempInputBufferString = new StringBuilder(inputBufferString.ToString());
            if (txtInput.Text.EndsWith("= "))
            {
                txtInput.Text = inputString.ToString();
            }
            //if (inputBufferString.ToString() == "0")
            //{
            //    inputBufferString.Clear();
            //    //return; //check effect
            //}
            if (inputBufferString.ToString().StartsWith("."))
            {
                inputBufferString = new StringBuilder("0" + inputBufferString.ToString());
            }
            if (sender is OperatorButton)
            {
                OperatorButton opButton = sender as OperatorButton;
                if (opButton.Text == "(")
                {
                    if (string.IsNullOrWhiteSpace(inputBufferString.ToString()))
                    {
                        inputBufferString.Append(opButton.Text);
                    }
                    else
                    {
                        inputBufferString.Append(string.Format("{0}{1}", customOperators.MultiplicationOperator.OperatorCode, opButton.Text));
                    }
                    inputBufferString = Logic.ValidateInputBuffer(inputBufferString.ToString()) ? inputBufferString : tempInputBufferString;

                    if (string.IsNullOrWhiteSpace(inputString.ToString()))
                    {
                        inputString.Append(inputBufferString.ToString());
                    }
                    else
                    {
                        inputString.Append(string.Format("{0}{1}", customOperators.MultiplicationOperator.OperatorCode, inputBufferString.ToString()));
                    }
                    txtInput.Text += string.Format("{0}{1}", tempInputBufferString.ToString(), opButton.Text);
                    inputBufferString.Clear();
                    txtInputBuffer.Text = "0.";
                    return;
                }
                if(txtInput.Text.EndsWith(")") || txtInput.Text.EndsWith("π"))
                {
                    inputString.Append(customOperators.MultiplicationOperator.OperatorCode);
                }

                //Create collection of prefix operations and test if op is one of them. If so append last result before appending op
                if (CustomOperators.PostOperandOperators.Contains(opButton.Operator) && (string.IsNullOrWhiteSpace(inputString.ToString())) && (string.IsNullOrWhiteSpace(inputBufferString.ToString())))
                {
                    inputBufferString.Append(string.Format("{0}{1}", Logic.Result, opButton.Operator.OperatorCode));
                }
                else
                {
                    inputBufferString.Append(opButton.Operator.OperatorCode);
                }

                inputBufferString = Logic.ValidateInputBuffer(inputBufferString.ToString()) ? inputBufferString : tempInputBufferString;
                inputString.Append(inputBufferString.ToString());
                inputBufferString.Remove(inputBufferString.Length - 1, 1);
                txtInput.Text += string.Format("{0}{1}", inputBufferString.ToString(), opButton.Operator.OperatorSymbol);
                inputBufferString.Clear();
                txtInputBuffer.Text = "0.";
            }
            else if (sender is ConstantButton)
            {
                ConstantButton constButton = sender as ConstantButton;
                if (string.IsNullOrWhiteSpace(inputBufferString.ToString()))
                {
                    inputBufferString.Append(constButton.Value.ToString());
                }
                else
                {
                    inputBufferString.Append(string.Format("{0}{1}", customOperators.MultiplicationOperator.OperatorCode, constButton.Value.ToString()));
                }
                inputBufferString = Logic.ValidateInputBuffer(inputBufferString.ToString()) ? inputBufferString : tempInputBufferString;
                inputString.Append(inputBufferString.ToString());
                txtInput.Text += string.Format("{0}{1}", tempInputBufferString.ToString(), constButton.Text);
                inputBufferString.Clear();
                txtInputBuffer.Text = "0.";
            }
            else if (sender is Button)
            {
                Button button = sender as Button;
                inputBufferString.Append(button.Text);
                inputBufferString = Logic.ValidateInputBuffer(inputBufferString.ToString()) ? inputBufferString : tempInputBufferString;
                txtInputBuffer.Text = (inputBufferString.ToString().Contains(".")) ? inputBufferString.ToString() : inputBufferString.ToString() + ".";
            }
            else
            {
                throw new Exception();
            }
            if (btnShift.IsSet)
            {
                ToggleShift();
            }
        }

        private void UpdateExpressionHistory(ExpressionType newExpression)
        {
            ExpressionsHistoryEntries.Add(newExpression);
            for (int counter = 0; counter < ExpressionsHistoryLabels.Count & counter <= ExpressionsHistoryEntries.Count - 1; counter++)
            {
                ExpressionsHistoryLabels[counter].Expression = ExpressionsHistoryEntries[ExpressionsHistoryEntries.Count - 1 - counter];
                ExpressionsHistoryLabels[counter].Text = ExpressionsHistoryLabels[counter].Expression.DisplayText;
            }
        }
        #endregion

        //Needs some work for improved user experience
        private void btnEquals_Click(object sender, EventArgs e)
        {
            if (btnShift.IsSet)
            {
                ConstantButton ansButton = new ConstantButton(Logic.Result);
                ansButton.Text = "Ans";
                AppendToInputString(ansButton);
                return;
            }
            Button senderButton = (Button)sender;
            if (Logic.ValidateInputBuffer(txtInputBuffer.Text))
            {
                if (txtInput.Text.EndsWith("= "))
                {
                    txtInput.Text = string.Empty;
                }
                if (txtInput.Text.EndsWith("M"))
                {
                    txtInputBuffer.Text = string.Empty;
                }
                if ((!string.IsNullOrWhiteSpace(inputBufferString.ToString())) && ((txtInput.Text.EndsWith(")") || txtInput.Text.EndsWith("π"))))
                {
                    inputString.Append(customOperators.MultiplicationOperator.OperatorCode);
                }

                if (txtInputBuffer.Text != "0.")
                {
                    inputString.Append(txtInputBuffer.Text.TrimEnd('.'));
                    txtInput.Text += txtInputBuffer.Text.TrimEnd('.');
                }
                if (senderButton.Text == "STO")
                {
                    txtInput.Text += " ⇒ M";
                }
                if(senderButton.Text == "M+")
                {
                    txtInput.Text += " ⇒ M+";
                }
                if(senderButton.Text == "M-")
                {
                    txtInput.Text += " ⇒ M-";
                }
                txtInput.Text += " = ";
                string result = string.Empty;

                try
                {
                    result = Logic.EvaluateExpression(inputString.ToString()).ToString();
                    txtInputBuffer.Text = result;
                }
                catch (Exception ex)
                {
                    txtInputBuffer.Text = ex.Message;
                    inputBufferString.Clear();
                    inputString.Clear();
                    return;
                }
                inputBufferString.Clear();

                ExpressionType expression = new ExpressionType();
                expression.DisplayText = txtInput.Text.Remove(txtInput.Text.IndexOf('=')).TrimEnd();
                expression.EvaluationText = inputString.ToString();
                expression.Result = Convert.ToDouble(result);
                UpdateExpressionHistory(expression);
                foreach (ExpressionHistoryLabel label in ExpressionsHistoryLabels)
                {
                    label.BackColor = Color.AliceBlue;
                }
                currentExpressionsHistoryItem = 0;
                inputString.Clear();
            }
            else
            {
                txtInputBuffer.Text = "Syntax error";
            }
            cursor = 0;
        }

        private void AllButtons_ShiftToggled(object sender, EventArgs e)
        {
            if (btnShift.IsSet)
            {
                foreach (OperatorButton button in OperatorButtons)
                {
                    button.Operator = button.ShiftOperator;
                    button.Text = button.Operator.OperatorCaption;
                }
                btnEquals.Font = new Font(btnDecimal.Font.FontFamily, 10);
                btnEquals.Text = "Ans";
                btnClear.Text = "RST";
                btnDecimal.Font = new Font(btnDecimal.Font.FontFamily, 8);
                btnDecimal.Text = "RND";
                btnStore.Text = "CLR";
                btnMemoryPlus.Text = "M-";
            }
            else
            {
                foreach (OperatorButton button in OperatorButtons)
                {
                    button.Operator = button.NormalOperator;
                    button.Text = button.Operator.OperatorCaption;
                }
                btnEquals.Font = new Font(btnDecimal.Font.FontFamily, 12);
                btnEquals.Text = "=";
                btnClear.Text = "C";
                btnDecimal.Font = new Font(btnDecimal.Font.FontFamily, 12);
                btnDecimal.Text = ".";
                btnStore.Text = "STO";
                btnMemoryPlus.Text = "M+";
            }
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            ToggleShift();
        }

        private void btnSimpleAppend_Click(object sender, EventArgs e)
        {
            AppendToInputString(sender);
        }

        private void btnCloseBracket_Click(object sender, EventArgs e)
        {
            if (!txtInput.Text.Contains("("))
            {
                return;
            }
            AppendToInputString(sender);
        }

        private void btnDecimal_Click(object sender, EventArgs e)
        {
            if (btnShift.IsSet)
            {
                double randomValue = new Random().NextDouble();
                ConstantButton randomValueButton = new ConstantButton(randomValue);
                randomValueButton.Text = "RANDOM";
                AppendToInputString(randomValueButton);
                return;
            }

            //if (inputBufferString.ToString() == "0")
            //{
            //    return;
            //}
            if (inputBufferString.ToString().Contains("."))
            {
                return;
            }
            AppendToInputString(sender);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            inputBufferString.Clear();
            if (txtInputBuffer.Text == "0.")
            {
                txtInput.Clear();
                inputString.Clear();
            }
            txtInputBuffer.Text = "0.";
            cursor = 0;
            if (btnShift.IsSet)
            {
                txtInput.Clear();
                inputString.Clear();
                Logic.Result = 0;
                foreach (ExpressionHistoryLabel label in ExpressionsHistoryLabels)
                {
                    label.Text = string.Empty;
                    label.BackColor = Color.AliceBlue;
                }
                ExpressionsHistoryEntries.Clear();
                ToggleShift();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (inputBufferString.Length > 0)
            {
                inputBufferString.Remove(inputBufferString.Length - 1, 1);
                txtInputBuffer.Text = inputBufferString.ToString();
            }
        }

        private void btnDRG_Click(object sender, EventArgs e)
        {
            int temp = (int)CustomOperators.currentDRG_Mode;
            CustomOperators.currentDRG_Mode = (++temp > (int)CustomOperators.DRG_Mode.Gradians) ? 0 : (CustomOperators.DRG_Mode)temp;
            if (CustomOperators.currentDRG_Mode == CustomOperators.DRG_Mode.Degrees)
            {
                lblDRG.Text = "DEG";
            }
            if (CustomOperators.currentDRG_Mode == CustomOperators.DRG_Mode.Radians)
            {
                lblDRG.Text = "RAD";
            }
            if (CustomOperators.currentDRG_Mode == CustomOperators.DRG_Mode.Gradians)
            {
                lblDRG.Text = "GRA";
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (++currentExpressionsHistoryItem > ExpressionsHistoryLabels.Count - 1)
            {
                //currentExpressionHistoryPage ++;
                ////currentExpressionHistoryPage = (currentExpressionHistoryPage > ExpressionsHistoryEntries.Count) ? ExpressionsHistoryEntries.Count : currentExpressionHistoryPage;
                //for(int i = NO_OF_EXPRESSION_HISTORY_DISPLAY_ITEMS - 1; (i >= 0) /*&& (i <= ExpressionsHistoryEntries.Count - /*currentExpressionHistoryPage * NO_OF_EXPRESSION_HISTORY_DISPLAY_ITEMS)*/; i--)
                //{
                //    ExpressionsHistoryLabels[i].Expression = ExpressionsHistoryEntries[ExpressionsHistoryEntries.Count - 1 - currentExpressionHistoryPage*NO_OF_EXPRESSION_HISTORY_DISPLAY_ITEMS + i];
                //}
                currentExpressionsHistoryItem = 0;
            }
            if (ExpressionsHistoryLabels[currentExpressionsHistoryItem].Text == string.Empty)
            {
                currentExpressionsHistoryItem = 0;
            }
            expressionHistoryLabel_Click(ExpressionsHistoryLabels[currentExpressionsHistoryItem], new EventArgs());
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (--currentExpressionsHistoryItem < 0)
            {
                currentExpressionsHistoryItem = ExpressionsHistoryLabels.Count - 1;
            }
            while (ExpressionsHistoryLabels[currentExpressionsHistoryItem].Text == string.Empty)
            {
                currentExpressionsHistoryItem--;
                if (currentExpressionsHistoryItem == 0)
                {
                    break;
                }
            }
            expressionHistoryLabel_Click(ExpressionsHistoryLabels[currentExpressionsHistoryItem], new EventArgs());
        }

        private void expressionHistoryLabel_Click(object sender, EventArgs e)
        {
            ExpressionHistoryLabel expressionHistoryLabel = (ExpressionHistoryLabel)sender;
            if (expressionHistoryLabel.Expression == null)
            {
                return;
            }
            foreach (ExpressionHistoryLabel label in ExpressionsHistoryLabels)
            {
                label.BackColor = Color.AliceBlue;
            }
            expressionHistoryLabel.BackColor = Color.PaleTurquoise;
            currentExpressionsHistoryItem = ExpressionsHistoryLabels.IndexOf(expressionHistoryLabel);
            txtInput.Text = expressionHistoryLabel.Expression.DisplayText + " = ";
            inputString.Clear();
            inputBufferString.Clear();
            cursor = 0;

            try
            {
                txtInputBuffer.Text = Logic.EvaluateExpression(expressionHistoryLabel.Expression.EvaluationText).ToString();
            }
            catch (Exception ex)
            {
                txtInputBuffer.Text = ex.Message;
            }
        }

        private void btnRecall_Click(object sender, EventArgs e)
        {
            if (!Logic.IsMemoryEngaged)
            {
                return;
            }
            ConstantButton memoryValueButton = new ConstantButton(Logic.Memory);
            memoryValueButton.Text = "M";
            AppendToInputString(memoryValueButton);
            txtInputBuffer.Text = memoryValueButton.Value.ToString();
        }

        private void btnStore_Click(object sender, EventArgs e)
        {
            if (btnShift.IsSet)
            {
                Logic.IsMemoryEngaged = false;
                Logic.Memory = 0;
                lblMemory.Visible = false;
                ToggleShift();
                return;
            }
            btnEquals_Click(sender, e);
            if (txtInputBuffer.Text != "Syntax error" || (!string.IsNullOrWhiteSpace(inputString.ToString()) && Convert.ToInt32(inputBufferString.ToString()) != 0))
            {
                Logic.Memory = Logic.Result;
                Logic.IsMemoryEngaged = true;
                lblMemory.Visible = true;
                txtInput.Text = string.Empty; //txtInput.Text.Remove(txtInput.Text.IndexOf("="));
            }
        }

        private void btnMemoryPlus_Click(object sender, System.EventArgs e)
        {
            if (btnShift.IsSet)
            {
                btnShift.IsSet = false;
                btnEquals_Click(sender, e);
                if (txtInputBuffer.Text != "Syntax error")
                {
                    Logic.Memory -= Logic.Result;
                    Logic.IsMemoryEngaged = true;
                    lblMemory.Visible = true;
                    txtInputBuffer.Text = Logic.Memory.ToString();
                    txtInput.Text = string.Empty;
                }
                btnShift.IsSet = true;
                ToggleShift();
            }
            else
            {
                btnEquals_Click(sender, e);
                if (txtInputBuffer.Text != "Syntax error")
                {
                    Logic.Memory += Logic.Result;
                    Logic.IsMemoryEngaged = true;
                    lblMemory.Visible = true;
                    txtInputBuffer.Text = Logic.Memory.ToString();
                    txtInput.Text = string.Empty;
                }
            }
            if(Logic.Memory == 0)
            {
                lblMemory.Visible = false;
                Logic.IsMemoryEngaged = false;
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtInput.Text)) //use txtInput.Text for test purposes
            {
                return;
            }
            cursor = (--cursor < 0) ? txtInput.Text.Trim().Length - 1 : cursor;
            cursor = (txtInput.Text.Trim()[cursor] == '=') ? --cursor : cursor;
            cursor = (txtInput.Text[cursor] == ' ') ? --cursor : cursor;

            txtInput.Focus();
            txtInput.Select(cursor, 1);
        }

        private void tspToggleDisplay_Click(object sender, EventArgs e)
        {
            if (tspToggleDisplay.Text == "▼")
            {
                tspToggleDisplay.Text = "▲";
                for(int i = this.Size.Height; i >= this.MinimumSize.Height; i -= 3)
                {
                    this.Size = new Size (Size.Width, i);
                }
            }
            else
            {
                tspToggleDisplay.Text = "▼";
                for (int i = this.Size.Height; i <= this.MaximumSize.Height; i += 5)
                {
                    this.Size = new Size(Size.Width, i);
                }
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtInput.Text)) //use txtInput.Text for test purposes
            {
                return;
            }
            cursor = (++cursor >= txtInput.Text.Length - 1) ? 0 : cursor;
            cursor = (txtInput.Text.Trim()[cursor + 1] == '=') ? 0 : cursor;
            cursor = (txtInput.Text[cursor] == ' ') ? ++cursor : cursor;

            txtInput.Focus();
            txtInput.Select(cursor, 1);

        }

        private void txtInputBuffer_TextChanged(object sender, EventArgs e)
        {
            //txtInputBuffer.Focus();
            if (txtInputBuffer.Text.Length > 0)
            {
                txtInputBuffer.Select(txtInputBuffer.Text.Length - 1, 0);
            }
            txtInputBuffer.ScrollToCaret();
        }

        private void txtInputBuffer_Enter(object sender, EventArgs e)
        {
            if (txtInputBuffer.Text == "0." || txtInputBuffer.Text == "0")
            {
                txtInputBuffer.Text = string.Empty;
            }
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            txtInput.Focus();
            if (txtInput.Text.Length > 0)
            {
                txtInput.Select(txtInput.Text.Length - 1, 0);
            }
            txtInput.ScrollToCaret();
            btnClear.Focus();
        }

        private void AllControls_KeyDown(object sender, KeyEventArgs e)
        {
            keyType = KeyType.Unknown;

            if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9) || e.KeyCode == Keys.Oemplus || e.KeyCode == Keys.Decimal)
            {
                keyType = KeyType.Normal;
            }
            else if(e.KeyCode == Keys.Add || e.KeyCode == Keys.Subtract || e.KeyCode == Keys.Multiply || e.KeyCode == Keys.Divide || e.KeyCode == Keys.OemOpenBrackets || e.KeyCode == Keys.OemCloseBrackets || e.KeyValue == 53)
            {
                keyType = KeyType.Operator;
            }
            //Remember to check for constants

            //    // Determine whether the keystroke is a number from the keypad.
            //    if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
            //    {
            //        // Determine whether the keystroke is a backspace.
            //        if (e.KeyCode != Keys.Back)
            //        {
            //            // A non-numerical keystroke was pressed.
            //            // Set the flag to true and evaluate in KeyPress event.
            //            keyType = true;
            //        }
            //    }
            //}
            ////If shift key was pressed, it's not a number.
            //if (Control.ModifierKeys == Keys.Shift)
            //{
            //    keyType = true;
            //}
        }

        private void AllControls_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
            // Check for the flag being set in the KeyDown event.
            switch (keyType)
            {
                case KeyType.Normal:
                    {
                        Button button = new Button();
                        button.Text = e.KeyChar.ToString();
                        AppendToInputString(button);
                        break;
                    }
                case KeyType.Operator:
                    {
                        OperatorButton operatorButton; 
                        if(e.KeyChar.ToString() == customOperators.AdditionOperator.OperatorCaption.Trim())
                        {
                            operatorButton = new OperatorButton(customOperators.AdditionOperator, customOperators.AdditionOperator);
                            AppendToInputString(operatorButton);
                        }
                        if (e.KeyChar.ToString() == customOperators.SubtractionOperator.OperatorCaption.Trim())
                        {
                            operatorButton = new OperatorButton(customOperators.SubtractionOperator, customOperators.SubtractionOperator);
                            AppendToInputString(operatorButton);
                        }

                        if (e.KeyChar.ToString() == customOperators.MultiplicationOperator.OperatorCaption.Trim())
                        {
                            operatorButton = new OperatorButton(customOperators.MultiplicationOperator, customOperators.MultiplicationOperator);
                            AppendToInputString(operatorButton);
                        }
                        if (e.KeyChar.ToString() == customOperators.DivisionOperator.OperatorCaption.Trim())
                        {
                            operatorButton = new OperatorButton(customOperators.DivisionOperator, customOperators.DivisionOperator);
                            AppendToInputString(operatorButton);
                        }
                        if (e.KeyChar.ToString() == customOperators.OpenBracketOperator.OperatorCaption.Trim())
                        {
                            operatorButton = new OperatorButton(customOperators.OpenBracketOperator, customOperators.OpenBracketOperator);
                            AppendToInputString(operatorButton);
                        }
                        if (e.KeyChar.ToString() == customOperators.CloseBracketOperator.OperatorCaption.Trim())
                        {
                            operatorButton = new OperatorButton(customOperators.CloseBracketOperator, customOperators.CloseBracketOperator);
                            AppendToInputString(operatorButton);
                        }
                        if (e.KeyChar.ToString() == customOperators.PercentageOperator.OperatorCaption.Trim())
                        {
                            operatorButton = new OperatorButton(customOperators.PercentageOperator, customOperators.PercentageOperator);
                            AppendToInputString(operatorButton);
                        }
                        break;
                    }

                default:
                        break;
                    }
            e.Handled = true;
        }
    }
}
